<?php
namespace Sample;
use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;

ini_set('error_reporting', E_ALL); // or error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');

class PayPalClient
{
    /**
     * Returns PayPal HTTP client instance with environment that has access
     * credentials context. Use this instance to invoke PayPal APIs, provided the
     * credentials have access.
     */
    public static function client()
    {
        return new PayPalHttpClient(self::environment());
    }

    /**
     * Set up and return PayPal PHP SDK environment with PayPal access credentials.
     * This sample uses SandboxEnvironment. In production, use LiveEnvironment.
     */
    public static function environment()
    {
        $clientId =  "AUQrmrUVa6NdEFO2GsYSaHQXtYmae5VvbaQbGfDjkxu6Ggc64SM0XNIf8S71Muy4mxBoS2hDi9YJlcML";
        $clientSecret =  "EHZnJe6DbvPOKXMJr5b5m5clEw-_V4LAwuzoS7wRdNPhOSESxxJmA8jdWKi7KE0xFUoayr2DD2ZvuObu"; 
        return new SandboxEnvironment($clientId, $clientSecret);
    }
}