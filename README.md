# README #

Setup : 
- Installer WAMP : https://www.wampserver.com/en/
- Installer Git : https://git-scm.com/
- Créer un dossier sous le dossier www de WAMP 
- Démarer WAMP
- Cloner le projet BitBucket dans le dossier sous www : 
	- git clone https://LudovicMigneault@bitbucket.org/LudovicMigneault/formulaire-reservation-souper.git
- Ouvrir le projet avec un IDE supportant GIT ( VS Code ou autre ) 
- Pull changes au début d'une session de travail
- Commit  / Push les changements régulièrement


### What is this repository for? ###

* Remplacer le formulaire JotForm de souper de https://chefnath.ca/r%C3%A9servations
* à faire en PHP et HTML

### Références ###

* Voir la documentation paypal developer PHP : 