<?php

namespace Sample\CaptureIntentExamples;

require __DIR__ . '/vendor/autoload.php';
//1. Import the PayPal SDK client that was created in `Set up Server-Side SDK`.
use Sample\PayPalClient;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;

session_start();
$_SESSION['commande'] = "<ul>";

$json = json_decode(file_get_contents('php://input'),true);
//print(round(calcTaxes(round(calcPrix(json_decode(file_get_contents('php://input'),true)),2)),2));
function calcPrix($req){
  $soupers = array(
    array("nom" => "Crème de légumes verts (1L)", "prix" => 9.2, "id"=>"plat1"),
      array("nom"=> "Salade d'orzo aux fruits de mer ( 2 unités )", "prix" => 34.5, "id"=>"plat2"),
      array("nom"=> "Jarret de porc nagano braisé", "prix" => 34.5, "id"=>"plat3"),
     array("nom"=> "Truffe au chocolat maison", "prix" => 2.88, "id"=>"plat4")
     );
  $sousTotal  = 0; 
//A VÉRIFIER SI C'EST LE BON PRIX!!!
  //a valider si le calcul des taxes se fait comme il faut
  foreach ($req as $cle=>$plat){
    if ($cle <= count($soupers)-1){
     $prix = ($soupers[$cle]['prix']);
     //$prix = "hello";
     if ($prix < 0 || empty($plat['prix'])){
      $prix = 0;
       //throw new Exception("le prix ne peut être négative");
    }
  
  
      $quantite = (int) $plat['qte'];
      //$quantite = "hello";
     if ($quantite < 0 || empty($plat['qte'])){

        $quantite = 0;
        //throw new Exception("La quantité ne peut être négative");
      }
      $sousTotal += floatval($prix * $quantite);
      $_SESSION['commande'] .= "<li> (".$quantite.") ".$soupers[$cle]["nom"]."</li>";

    
  }}
      $_SESSION['commande'] .= "</ul>";
      $_SESSION['nom'] = $req[count($soupers)]['nom'];
      $_SESSION['courriel'] = $req[count($soupers) + 1]['courriel'];
      $_SESSION['tel'] = $req[count($soupers) + 2]['Numéro de téléphone'];
      $_SESSION['date'] = $req[count($soupers) + 3]['Date'];
      $_SESSION['Adresse'] = $req[count($soupers) + 4]['Adresse'];


     return $sousTotal;
};

function calcTaxes($sousTotal){
  return (0.0975 * $sousTotal + 0.05* $sousTotal);
}


class CreateOrder
{

// 2. Set up your server to receive a call from the client
  /**
   *This is the sample function to create an order. It uses the
   *JSON body returned by buildRequestBody() to create an order.
   */
  public static function createOrder($debug=false)
  {
    $request = new OrdersCreateRequest();
    $request->prefer('return=representation');
    $request->body = self::buildRequestBody();

    //print (json_encode($request->body));
    
    


   // 3. Call PayPal to set up a transaction
    $client = PayPalClient::client();
    $response = $client->execute($request);
    echo json_encode($response->result, JSON_PRETTY_PRINT);
    if ($debug)
    {
      print "Status Code: {$response->statusCode}\n";
      print "Status: {$response->result->status}\n";
      print "Order ID: {$response->result->id}\n";
      print "Intent: {$response->result->intent}\n";
      print "Links:\n";
      foreach($response->result->links as $link)
      {
        print "\t{$link->rel}: {$link->href}\tCall Type: {$link->method}\n";
      }

      // To print the whole response body, uncomment the following line
      echo json_encode($response->result, JSON_PRETTY_PRINT);
    }

    // 4. Return a successful response to the client.
    return $response;
}

  /**
     * Setting up the JSON request body for creating the order with minimum request body. The intent in the
     * request body should be "AUTHORIZE" for authorize intent flow.
     *
     */
    private static function buildRequestBody()
    {
        return array(
            'intent' => 'CAPTURE',
            'application_context' =>
                array(
                    'return_url' => 'http://chefnath.ca/reservation/succes.php',
                    'cancel_url' => 'http://chefnath.ca/reservation/cancel.php'
                ),
            'purchase_units' =>
                array(
                    0 =>
                        array(
                            'amount' =>
                                array(
                                    'currency_code' => "CAD",
                                    'value' => number_format(round(calcPrix(json_decode(file_get_contents('php://input'),true)),2) + round(calcTaxes(round(calcPrix(json_decode(file_get_contents('php://input'),true)),2)),2),2),
                                    'breakdown' => 
                                      array(
                                    'item_total' => 
                                        array(
                                          'currency_code' => "CAD",
                                          'value' => number_format(calcPrix(json_decode(file_get_contents('php://input'),true)),2),
                                        ),

                                    'tax_total' =>
                                      array(
                                        'currency_code' => "CAD",
                                        'value' => number_format(calcTaxes(round(calcPrix(json_decode(file_get_contents('php://input'),true)),2)),2),
                                      ))
                        )
                )
                                )
                              );
    }
}


/**
 *This is the driver function that invokes the createOrder function to create
 *a sample order.
 */

  CreateOrder::createOrder(false);

?>